#!/bin/bash
set -x
curl -so /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo 
yum install -y python-pip
yum install libevent-devel openssl-devel libffi-devel python-devel gcc-c++ -y
mkdir /root/.pip
tee /root/.pip/pip.conf << EOF 
[global]
index-url = http://pypi.douban.com/simple
[install]
trusted-host=pypi.douban.com
EOF
pip install -U pip
pip install -U ansible==2.2.1.0

