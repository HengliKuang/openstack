#!/bin/bash
system_status=`egrep -c '(vmx|svm)' /proc/cpuinfo`
if [ "$system_status" -eq 0 ];then
       sed -i 's/virt_type\ =\ kvm/virt_type=qemu/g' /etc/nova/nova.conf 
else
       exit 0
fi
